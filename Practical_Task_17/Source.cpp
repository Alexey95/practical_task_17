#include <iostream>
#include <math.h>

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() :x(5), y(2), z(1)
	{}

	Vector(double _x, double _y, double _z) :x(_x), y(_y), z(_z)
	{}


	void Show()
	{
		std::cout << sqrt(x * x + y * y + z * z) << std::endl;
	}

};

int main()
{
	Vector V;
		Vector V2(8, 6, 5);
		Vector V3(9, 7, 4);
		
		V.Show();
		V2.Show();
		V3.Show();

		return 0;
}